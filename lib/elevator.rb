require "elevator/version"
require "elevator/groups"

module Elevator


  def self.run
    systems = Groups.new(elevators: 3, floors: 10)
    systems.elevator_request(floor: 5)
    systems.elevator_request(floor: 9)
    systems.elevator_request(floor: 2)
  end
end
