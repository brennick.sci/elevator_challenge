require "elevator/system"

module Elevator
  class Groups

    def initialize(elevators: 3, floors: 10)
      names = ('A'..'Z').to_a
      @systems = (1..elevators).map { System.new(floors: floors, name: names.sample) }
    end

    def elevator_request(floor: 1)
      system = closest_elevator_to(floor)
      puts "Closest elevator #{system.name} from floor #{system.current_floor}"
      system.elevator_request(floor: floor)
    end

    def closest_elevator_to(floor)
      closest = @systems.first
      @systems.each do |system|
        closest = system.num_floors_away(floor) < closest.num_floors_away(floor) ? system : closest
      end
      closest
    end

  end
end
