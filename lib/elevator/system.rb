module Elevator
  class System

    attr_reader :name, :current_floor

    def initialize(floors: 10, name: 'N/A')
      @name = name
      @floors = floors # total number of floors
      @requests = [] # floors requested to move to
      @current_floor = [1,5,10].sample # floor currently on
      @direction = :up # FIXME default direction, don't need, remove
    end

    def elevator_request(floor: 1)
      if valid_floor?(floor)
        @requests << floor
        move_to_next_floor
      end
    end

    def floor_request(floor)
      if valid_floor?(floor)
        @requests << floor
        move_to_next_floor
      end
    end

    def move_to_next_floor
      plan_moves
      while can_move_floors?
        move_to = next_floor
        moves = make_moves(move_to)
        moves.each do |move|
          time_passed
          puts "Moving to Floor: #{move}"
        end
        @current_floor = move_to
        puts "Stopped on Floor: #{@current_floor}"
      end
    end

    def make_moves(move_to)
      curr_floor = @current_floor > 1 ? @current_floor - 1 : 1
      if move_to < curr_floor # move down
        (move_to..curr_floor).to_a.reverse
      elsif move_to > curr_floor # move up
        (curr_floor..move_to).to_a
      else
        []
      end
    end

    def can_move_floors?
      !@requests.empty?
    end

    def next_floor
      if @direction == :up
        @requests.shift
      else
        @requests.pop
      end
    end

    def plan_moves
      if @direction == :up
        @requests.sort!.uniq!
      else
        @requests.sort!.uniq!.reverse!
      end
    end

    def num_floors_away(requested_floor)
      (requested_floor - @current_floor).abs
    end

    private

    def time_passed
      sleep 0.25
    end

    def valid_floor?(floor)
      floor >= 1 && floor <= @floors
    end

    def valid_direction?(direction)
      direction == :up || direction == :down
    end
  end
end
